//
//  Models.swift
//  CeriosDrivr
//
//  Created by Rey Cerio on 2017-02-28.
//  Copyright © 2017 CeriOS. All rights reserved.
//

import UIKit

//class Driver: NSObject {
//    var userId: String?
//    var name: String?
//    var email: String?
//    var phone: String?
//    var trackerPhone: String?
//    
//    init(userId: String, name: String, email: String, phone: String, trackerPhone: String) {
//        self.userId = userId
//        self.name = name
//        self.phone = phone
//        self.trackerPhone = phone
//    }
//}


class DriverLocation: NSObject {
    var date: String?
    var latitude: String?
    var longitude: String?
    var uid: String?
}

struct Message {
    var date: String?
    var dispatcherId: String?
    var dispatcherName: String?
    var message: String?
}

struct Dispatcher {
    var userId: String?
    var name: String?
    var email: String?
    var phone: String?
}

struct Driver {
    var userId: String?
    var name: String?
    var email: String?
    var phone: String?
    var trackerPhone: String?
}
